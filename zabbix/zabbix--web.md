## zabbix 部署
## 选择是nginx做代理
## server 端
### zabbix 源
```
rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/8/x86_64/zabbix-release-5.0-1.el8.noarch.rpm

```
### zabbix epel依赖
```

yum install -y epel-release.noarch

```

### 配置mariadb源
```
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.3/centos8-amd64
module_hotfixes=1
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1

```
### 下载zabbix相关软件
```
dnf install zabbix-server-mysql zabbix-web-mysql zabbix-nginx-conf zabbix-agent zabbix-get zabbix-sender

```
### 下载mariadb
```
yum install -y mariadb-server

```

### 启动数据库mariadb 
```
systemctl start mariadb
systemctl enable mariadb
可能报错
```
### mariadb可能字符集报错
```
rpm -qa  | grep MariaDB
删除下面全部安装的

用yum remove 名字
把/cd /var/lib/mysql 把mysql文件备份一下
然后全部删除mysql里面文件
/var/lib
cp mysql -r mysql.ak
进入mysql删除全部
rm -rf *
yum search mariadb
安装yum install -y mariadb-server.x86_64 mariadb-server-utils.x86_64
```
### 改utf8 路径/etc/my.cnf.d
```
vim server.cnf.rpmsave
vim mariadb-server.cnf
vim client.cnf
vim client.cnf.rpmsave
```
![如图](./img/1.png)

![如图](./img/2.png)

![如图](./img/3.png)

![如图](./img/4.png)

### 进入mariadb
```
\s 可以看到下面四行
Server characterset:	utf8
Db     characterset:	utf8
Client characterset:	utf8
Conn.  characterset:	utf8

创建数据库
create database zabbix character set utf8 collate utf8_bin;
授权网段
grant all on zabbix.* to 'zbxuser'@'47.115.79.9.%' identified by 'zabbix';

grant all on zabbix.* to 'zbxuser'@'127.0.0.1%' identified by 'zabbix';

grant all on zabbix.* to 'zbxuser'@'localhost' identified by 'zabbix';
刷新授权
flush privileges;
```
### 导入zabbix初始的表
```
进入/usr/share/doc/zabbix-server-mysql
可能有时候这个文件夹没有 create.sql.gz没有的话find 找一下
zcat create.sql.gz | mysql -u zbxuser -pzabbix -h127.0.0.1 zabbix
```
### vim /etc/zabbix/zabbix_server.conf
```
ListenPort=10051
SourceIP=47.115.79.9
DBHost=localhost
DBName=zabbix
DBUser=zbxuser
 DBPassword=zabbix
 DBPort=3306
```
### nginx vim /etc/nginx/conf.d/zabbix.conf 
```
更改前两行配置
直接访问网址，安装即可
如果报错时区
vim /etc/php.ini 
date.timezone =Asia/Shanghai
```
